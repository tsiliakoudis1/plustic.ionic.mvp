import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
//import {AdMobService} from '../../providers/ad-mob/ad-mob.service'
/**
 * Generated class for the SubmitmodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-submitmodal',
  templateUrl: 'submitmodal.html',
})
export class SubmitmodalPage {
  n1: number;
  n2: number;
  n3: number;

  constructor( private navCtrl: NavController, private navParams: NavParams, private viewCtrl: ViewController) {

    this.n1 = navParams.get('n1');
    this.n2 = navParams.get('n2');
    this.n3 = navParams.get('n3');
  }

    dismiss() {
    this.viewCtrl.dismiss();
    //this.adMob.showInterstitial();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubmitmodalPage');
  }

}
