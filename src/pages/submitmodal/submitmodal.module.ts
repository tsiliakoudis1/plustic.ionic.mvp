import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubmitmodalPage } from './submitmodal';

@NgModule({
  declarations: [
    SubmitmodalPage,
  ],
  imports: [
    IonicPageModule.forChild(SubmitmodalPage),
  ],
})
export class SubmitmodalPageModule {}
