import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactPage } from './contact';
import { IonicStorageModule } from '@ionic/storage'

@NgModule({
  declarations: [
    ContactPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactPage),
    IonicStorageModule.forRoot()
  ],
})
export class ContactPageModule {}
