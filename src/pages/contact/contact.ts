import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Platform } from 'ionic-angular';
//import { SMS } from 'ionic-native';
import { Storage } from '@ionic/storage';

declare var navigator : any;

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  isContactAccessConfirmed: boolean = false;
  isFetching: boolean = false;
  isInit: boolean = false;
  contacts: Array<any> = [];
  groupedContacts: Array<any> = [];

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public platform: Platform, public storage: Storage) {
   
    this.storage.get('isContactAccessConfirmed').then((val) => {
      this.isContactAccessConfirmed = val;
      this.isInit = true;
      if (this.isContactAccessConfirmed) {
        this.fetchContacts(null);
      }
    })

    
  }

  dismiss() {
    this.navCtrl.pop();
  }

  confirm() {
    let that = this;
    this.fetchContacts(function() {
      that.isContactAccessConfirmed = true;
      that.storage.set('isContactAccessConfirmed', true);
    });
  }

/*   invite(contact: any) {
    if (contact.invited) {
      return;
    }
    SMS.send(contact.number, 'Hey, come join me on my app! You can download it at: http://t.co/123456789', {
      android : {
        intent: 'INTENT'
      }
    }).then(function() {
      contact.invited = true;
    }, function(error) {
      console.log(error);
    });
  } */

  fetchContacts(cb) {
    let loader = this.loadingCtrl.create({content: "Φόρτωση επαφών.."});
    this.platform.ready().then((readySource) => {
      loader.present();
      this.isFetching = true;
      let that = this;
      navigator.contactsPhoneNumbers.list(function(contacts) {
        console.log(contacts.length + ' contacts found');
        for(var i = 0; i < contacts.length; i++) {
           console.log(contacts[i].id + " - " + contacts[i].displayName);
           for(var j = 0; j < contacts[i].phoneNumbers.length; j++) {
              var phone = contacts[i].phoneNumbers[j];
              console.log("===> " + phone.type + "  " + phone.number + " (" + phone.normalizedNumber+ ")");
              that.contacts.push({
                name: contacts[i].displayName,
                number: phone.normalizedNumber,
                invited: false
              });
           }
        }
        that.groupContacts(that.contacts);
        that.isFetching = false;
        loader.dismiss();
        cb && cb();
      }, function(error) {
        console.error(error);
        that.isFetching = false;
        loader.dismiss();
      });
    });
  }

  searchContacts(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() !== '') {
      this.groupContacts(this.contacts.filter((contact) => {
        return contact.name.toLowerCase().indexOf(val.toLowerCase()) > -1 || 
          contact.number.toLowerCase().indexOf(val.toLowerCase()) > -1;
      }));
    }
    else {
      this.groupContacts(this.contacts);
    }
  }

  groupContacts(contacts){
    this.groupedContacts = []; // init grouped list
    // sort all contacts by display name
    let sortedContacts = contacts.sort(function(a,b) {
      var x = a.name.toLowerCase();
      var y = b.name.toLowerCase();
      return x < y ? -1 : x > y ? 1 : 0;
    });
    let currentLetter = false;
    let currentContacts = [];
    // assign each contact to a group
    sortedContacts.forEach((value, index) => {
        if(value.name.charAt(0) != currentLetter){
            currentLetter = value.name.charAt(0);
            let newGroup = {
                letter: currentLetter,
                contacts: []
            };
            currentContacts = newGroup.contacts;
            this.groupedContacts.push(newGroup);
        } 
        currentContacts.push(value);
    });
   }
}
