import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from "../../providers/auth/auth.service";
import { DataService } from "../../providers/data/data.service";
import { User } from "firebase/app";
import { Profile } from "../../models/profile/profile.interface";
import { FirebaseListObservable } from 'angularfire2/database';
import { Ticket } from "../../models/ticket/ticket.interface";
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';


@IonicPage()
@Component({
  selector: 'page-coupons',
  templateUrl: 'coupons.html',
})
export class CouponsPage {


  pendingTicketList: {};
  pastTicketList: {};

  private authenticatedUser$: Subscription;
  private authenticatedUser: User;
  coins;
  ticketType: string = "pendingTickets";


  //@Output() existingProfile: EventEmitter<Profile>;

  constructor(private navCtrl: NavController, private navParams: NavParams, private data: DataService, private auth: AuthService) {
    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.authenticatedUser = user;
    })
    //this.existingProfile = new EventEmitter<Profile>();
  }

  ionViewWillLoad() {
    this.getPendingTickets();
    this.getCoins();
    console.log('ionViewWillLoad CouponsPage');
  }
  ionViewDidLoad() {
    this.getPastTickets();
  }

  async getPendingTickets() {
    if (this.authenticatedUser) {
      this.pendingTicketList = this.data.getPendingTicketListRef(this.authenticatedUser)
    }
  }

  async getPastTickets() {
    if (this.authenticatedUser) {
      this.pastTicketList = this.data.getPastTicketListRef(this.authenticatedUser)
    }
  }

  async getCoins() {
    if (this.authenticatedUser) {
      this.coins = this.data.getCoinsListRef(this.authenticatedUser)
    }
  }

  navigateToDraw(ticket: Ticket): void {
    this.navCtrl.push('TicketSelectionPage', { ticket });
  }

}
