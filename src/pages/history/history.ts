import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Transaction } from '../../models/transaction.interface';


@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  transactions: Transaction[] = [
    {
      "ammount": 100.35,
      "type": "pay",
      "card": "ΑΒ Βασιλόπουλος",
      "company": "ΑΒ Βασιλόπουλος",
      "date": "23/05",
      "time": "10:36",
    },
    {
      "ammount": 56,
      "type": "pay",
      "card": "PlusCard",
      "company": "Cosmote",
      "date": "23/05",
      "time": "10:36",
    },
    {
      "ammount": 30.40,
      "type": "refund",
      "card": "Dine",
      "company": "Γρηγόρης",
      "date": "22/05",
      "time": "12:09",
    },
    {
      "ammount": 16.40,
      "type": "pay",
      "card": "Dine",
      "company": "My Market",
      "date": "23/05",
      "time": "17:36",
    },

  ]


  constructor(private navCtrl: NavController, private navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

  goToTransaction(event, transaction: Transaction){
    console.log(transaction)
    }

}
