import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubmitExchangeModalPage } from './submit-exchange-modal';

@NgModule({
  declarations: [
    SubmitExchangeModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SubmitExchangeModalPage),
  ],
})
export class SubmitExchangeModalPageModule {}
