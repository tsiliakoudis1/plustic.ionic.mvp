import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the SubmitExchangeModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-submit-exchange-modal',
  templateUrl: 'submit-exchange-modal.html',
})
export class SubmitExchangeModalPage {

  photo: string;
  cost: number;
  title: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
    this.photo = navParams.get('photo');
    this.title = navParams.get('title');
    this.cost = navParams.get('cost');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubmitExchangeModalPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
