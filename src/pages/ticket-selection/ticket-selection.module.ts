import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicketSelectionPage } from './ticket-selection';

@NgModule({
  declarations: [
    TicketSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(TicketSelectionPage),
  ],
})
export class DrawPageModule {}
