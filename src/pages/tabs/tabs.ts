import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root: string;
  tab2Root: string;
  tab3Root: string;

  constructor() {
    this.tab1Root = 'HomePage';
    //this.tab1Root = 'PhoneInputPage';
    this.tab2Root = 'AccountPage'
    //this.tab3Root = 'AccountPage'
  }

 

}
