import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Notification } from '../../models/notification.interface';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})



export class NotificationsPage {

  notifications: Notification[] = [
    {
     "avatar": "assets/img/profile_pic.jpg",
      "name": "Plustic",
      "content": "Ut interdum vehicula ante aliquam luctus. Vivamus et turpis ut nibh euismod pulvinar.",
      "date": "22/05"
    },
    {
      "avatar": "assets/img/profile_pic.jpg",
       "name": "Plustic",
       "content": "Cras cursus, magna vel vulputate consectetur, sapien lectus consectetur quam, non tincidunt sem mi vehicula nisi",
       "date": "23/05"
     },
     {
      "avatar": "assets/img/profile_pic.jpg",
       "name": "Plustic",
       "content": "Pellentesque posuere blandit libero ut molestie. Duis vel vehicula eros.",
       "date": "24/05"
     }
  ]

  constructor(private navCtrl: NavController, private navParams: NavParams) {
  }



goToNotification(event, notification: Notification){
console.log(notification)
}
}
