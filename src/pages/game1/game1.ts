import { Component, OnDestroy } from '@angular/core';
import { IonicPage, ToastController, ModalController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth.service';
import { DataService } from '../../providers/data/data.service';
import { Ticket } from "../../models/ticket/ticket.interface";
import { User } from 'firebase/app';
import { Subscription } from 'rxjs/Subscription';
import * as firebase from 'firebase';
//import {AdMobService} from '../../providers/ad-mob/ad-mob.service'


@IonicPage()
@Component({
  selector: 'page-game1',
  templateUrl: 'game1.html',
})


export class Game1Page {
  numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  private n1: number;
  private n2: number;
  private n3: number;
  private n1display = false;
  private n2display = false;
  private n3display = false;
  ticket = {} as Ticket;
  private authenticatedUser$: Subscription;
  private authenticatedUser: User;
  coins;
  loader: Loading;


  constructor(private toast: ToastController, private modal: ModalController,
    private data: DataService, private auth: AuthService, private loading: LoadingController) {

    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.authenticatedUser = user;
    })
    //this.adMob.removeBanner();
  }

  ionViewWillLoad() {
    this.getCoins();
    console.log('ionViewWillLoad Game1Page');
  }

  ngOnDestroy(): void {
    //this.adMob.showBannerAd();
  }

  async getCoins() {
    if (this.authenticatedUser) {
      this.coins = this.data.getCoinsListRef(this.authenticatedUser)
    }
  }

  selectn1(number: number) {
    this.n1display = true;
    this.n1 = (number);
    console.log(this.n1);
  }

  selectn2(number: number) {
    this.n2display = true;
    this.n2 = (number);
    console.log(this.n2);
  }
  selectn3(number: number) {
    this.n3display = true;
    this.n3 = (number);
    console.log(this.n3);
  }



  async submitTicket() {
    console.log(this.n1, this.n2, this.n3);

    if (this.authenticatedUser) {
      this.ticket.n1 = this.n1;
      this.ticket.n2 = this.n2;
      this.ticket.n3 = this.n3;
      this.ticket.coinsWon = 0;
      this.ticket.status = 'future'
      this.ticket.game = 'lucky3';
      this.ticket.createdAt = firebase.database.ServerValue.TIMESTAMP;
      this.ticket.date = Date.now()

      const result = await this.data.submitTicket(this.authenticatedUser, this.ticket)
      
      this.loader = this.loading.create({
        content: 'Submiting your ticket..',
        duration: 2500,
      }); this.loader.present();

      this.loader.onDidDismiss(() => {

        if (result) {
          const myModal = this.modal.create('SubmitmodalPage', { 'n1': this.n1, 'n2': this.n2, 'n3': this.n3 });
          myModal.present();
          console.log(this.ticket)
          this.n1 = null
          this.n2 = null
          this.n3 = null
          this.n1display = false;
          this.n2display = false;
          this.n3display = false;
        }
        else {
          this.toast.create({
            message: `Ticket not submitted. Please try again`,
            duration: 3000
          }).present();
        }

      });






    }
  }
}

