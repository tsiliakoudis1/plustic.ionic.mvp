import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GetCoinsPage } from './get-coins';

@NgModule({
  declarations: [
    GetCoinsPage,
  ],
  imports: [
    IonicPageModule.forChild(GetCoinsPage),
  ],
})
export class GetCoinsPageModule {}
