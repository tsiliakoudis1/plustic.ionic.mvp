import { Component } from '@angular/core';
import { IonicPage, NavController} from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { User } from 'firebase/app';
import { DataService } from '../../providers/data/data.service';
import { AuthService } from '../../providers/auth/auth.service';
//import { AdMobService } from '../../providers/ad-mob/ad-mob.service';

/**
 * Generated class for the GetCoinsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-get-coins',
  templateUrl: 'get-coins.html',
})
export class GetCoinsPage {

  coins;
  private authenticatedUser$: Subscription;
  private authenticatedUser: User;


  constructor(private navCtrl: NavController, private data: DataService,
    private auth: AuthService) {

    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.authenticatedUser = user;
    })
  }

  ionViewWillLoad() {
   
    console.log('ionViewDidLoad GetCoinsPage');
 
  }

 
  


}

