import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from "../../providers/auth/auth.service";
import { DataService } from "../../providers/data/data.service";
import { Subscription } from 'rxjs/Subscription';
import { User } from 'firebase/app';
import { BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult } from '@ionic-native/barcode-scanner'
import { Toggle } from 'ionic-angular/components/toggle/toggle';
import { AlertController } from 'ionic-angular';
import {trigger, state, style,  transition, animate } from '@angular/animations'
import { PosRequest } from '../../models/posRequest.interface';
//import {AdMobService} from '../../providers/ad-mob/ad-mob.service'

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
trigger('openCard',[
  state('open', style({
    transform: 'translateY(+400px)',
    
  })),
  state('normal', style({

  })),
  transition('* => *'  , animate('0.1s'))
])
  ]
})

export class HomePage {

  posRequest = {} as PosRequest;
  ammount: number;
  wallet: string = "cards";
  openState = 'normal'
  credits;
  card: number;
  open: boolean = false;
  openedCard: string;
  payCode;
  owned;
  val;
  private authenticatedUser$: Subscription;
  private authenticatedUser: User;
  loader: Loading;  
result: BarcodeScanResult;
cards: any[] = [
  {
    "logo": "assets/test-img/pluscard.svg",
    "image": "assets/test-img/plus.png",
    "color": "white",
    "bgColor": "white",
    "balance": "425.40",
  },
  {
    "logo": "assets/test-img/myMarket.png",
    "image": "",
    "color": "black",
    "bgColor": "white",
    "balance": "90.80",
  },
  {
    "logo": "assets/test-img/Greg.png",
    "image": "assets/test-img/food.jpg",
    "color": "white",
    "bgColor": "white",
    "balance": "32",
    
  },
  {
    "logo": "assets/test-img/travel.svg",
    "image": "",
    "color": "black",
    "bgColor": "white",
    "balance": "107.50",
  },
  {
    "logo": "assets/test-img/bonus.svg",
    "image": "",
    "color": "black",
    "bgColor": "white",
    "balance": "25",
  }
];

  constructor(private navCtrl: NavController, private navParams: NavParams, private data: DataService,
    private auth: AuthService, private loading: LoadingController, private barcode: BarcodeScanner, private alertCtrl: AlertController) {

    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.authenticatedUser = user;
    });



  }

  async scanBarcode() {
    try {
      const options: BarcodeScannerOptions = {
        torchOn: false,
        disableSuccessBeep: true
      }

      this.result = await this.barcode.scan(options)
      let alert = this.alertCtrl.create({
        title: `${this.result.format}` ,
        subTitle: `${this.result.text}`,
        buttons: ['OK']
      });
      alert.present();

    }
    catch (error) {
      console.log(error);
    }
  }


  navigateToPage(pageName: string) {
    this.navCtrl.push(pageName);
  }

  ionViewWillLoad() {
    this.getCoins();
    console.log('ionViewWillLoad HomePage');
  };

  ionViewDidLoad() {
  };

  async getCoins() {
    if (this.authenticatedUser) {
      this.credits = this.data.getCoinsListRef(this.authenticatedUser);
      this.owned = this.data.getCoinsListRef(this.authenticatedUser).subscribe(val => {
        console.log(val)
        this.owned = val;
        this.payCode = this.owned.paycode;
      });
    }
  }

  tap($event){
    
  }

  openCard(i){
    this.card = i;
    console.log(this.card)
    
  }

  openTest(){
this.openState = (this.openState == 'open')? 'normal' : 'open';
  }


  setSegment(segment){
    this.wallet = segment;
  }

async generateQR(ammount){

  const data = {
    Εταιρεία : "testCompany",
    Ποσό: ammount
  }
  try {
  await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, data)
}
catch(error){
  console.log(error);
}
}


}
