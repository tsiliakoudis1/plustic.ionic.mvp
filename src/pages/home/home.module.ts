import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { ComponentsModule } from "../../components/components.module";
import {BarcodeScanner } from '@ionic-native/barcode-scanner'

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    ComponentsModule
  ],
  providers: [
    BarcodeScanner
  ],
})
export class HomePageModule {}
