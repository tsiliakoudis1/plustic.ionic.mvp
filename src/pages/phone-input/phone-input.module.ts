import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhoneInputPage } from './phone-input';
import { AngularFireAuthModule } from 'angularfire2/auth';

@NgModule({
  declarations: [
    PhoneInputPage,
  ],
  imports: [
    IonicPageModule.forChild(PhoneInputPage),
    AngularFireAuthModule
  ],
})
export class PhoneInputPageModule {}
