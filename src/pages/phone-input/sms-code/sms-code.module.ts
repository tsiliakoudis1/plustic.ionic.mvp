import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SmsCodePage } from './sms-code';

@NgModule({
  declarations: [
    SmsCodePage,
  ],
  imports: [
    IonicPageModule.forChild(SmsCodePage),
  ],
})
export class SmsCodePageModule {}
