import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-sms-code',
  templateUrl: 'sms-code.html',
})
export class SmsCodePage {

  code: any;
  phone: number;

  constructor(private navCtrl: NavController, private navParams: NavParams) {
    this.phone = navParams.get('phoneNo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SmsCodePage');
  }

  submit (code) {
    console.log(code)
    console.log(this.phone)
  }
}
