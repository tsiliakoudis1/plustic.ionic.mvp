import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Profile } from '../../models/profile/profile.interface'
import { AuthService } from '../../providers/auth/auth.service'
import { AngularFireAuth } from 'angularfire2/auth';
import { LoginResponse } from '../../models/login/login-response.interface';
import firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-phone-input',
  templateUrl: 'phone-input.html',
})
export class PhoneInputPage {

  phoneNo;
  account = {} as Account
  public recaptchaVerifier;

  constructor(private navCtrl: NavController, private navParams: NavParams, private auth: AngularFireAuth) {
    this.auth.auth.useDeviceLanguage();

    
  }

  ionViewDidLoad() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible',
      'callback': function(response) {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        //onSignInSubmit();
      }
    });
  }

submit(phone){
  this.navCtrl.push('SmsCodePage', {phoneNo: phone });
  console.log(phone)
}

  async signInWithPhone(phone: number) {
    this.phoneNo = '+30' + phone
    console.log(this.phoneNo)
    console.log(phone)
    try {
      return {
        result: await this.auth.auth.signInWithPhoneNumber(this.phoneNo, this.recaptchaVerifier).then(function (result){
          console.log(result)
        })
      }
    }
    catch (e) {
      return {
        error: e
      };
    }
  }




}
