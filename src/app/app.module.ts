import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { MyApp } from './app.component';
import { FIREBASE_CONFIG } from './app.firebase.config'
import { AuthService } from '../providers/auth/auth.service';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { DataService } from '../providers/data/data.service';
import { FormsModule } from '@angular/forms'
import { Push } from '@ionic-native/push';
import { Contacts } from '@ionic-native/contacts'

/* import { AdMobFree } from '@ionic-native/admob-free';
import { AdMobService } from '../providers/ad-mob/ad-mob.service'; */

@NgModule({
  declarations: [
    MyApp,
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    DataService,
    Push,
    Contacts,
    //AdMobFree,
    //AdMobService, 
  ]
})
export class AppModule {}
