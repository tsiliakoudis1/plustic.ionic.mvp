import { Component } from '@angular/core';

/**
 * Generated class for the UserCoinsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'user-coins',
  templateUrl: 'user-coins.html'
})
export class UserCoinsComponent {

  text: string;

  constructor() {
    console.log('Hello UserCoinsComponent Component');
    this.text = 'Hello World';
  }

}
