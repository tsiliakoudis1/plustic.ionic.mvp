import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';
import { User } from 'firebase/app';
import { Profile } from '../../models/profile/profile.interface';
import { Ticket } from '../../models/ticket/ticket.interface';
import "rxjs/add/operator/take";
import "rxjs/add/operator/map";
import "rxjs/add/operator/mergeMap";
import { AuthService } from "../auth/auth.service";
import { Product } from '../../models/product/product.interface';

@Injectable()
export class DataService {

  profileObject: FirebaseObjectObservable<Profile>
  ticketObject: FirebaseObjectObservable<Ticket>
  productObject: FirebaseObjectObservable<any>
  drawObject;

  constructor(private database: AngularFireDatabase, private authService: AuthService) {
  }

  /////////////////////////////////
  //////// User Data //////////////
  /////////////////////////////////
  getAuthenticatedUserProfile() {
    return this.authService.getAuthenticatedUser()
      .map(user => user.uid)
      .mergeMap(authId => this.database.object(`/profiles/${authId}`))
      .take(1)
  }

  getProfile(user: User) {
    this.profileObject = this.database.object(`/profiles/${user.uid}`, { preserveSnapshot: true });
    return this.profileObject.take(1);
  }

  async saveProfile(user: User, profile: Profile) {
    this.profileObject = this.database.object(`/profiles/${user.uid}`);

    try {
      await this.profileObject.set(profile);
      return true;
    }
    catch (e) {
      console.error(e);
      return false;
    }
  }


  /////////////////////////////////
  //////// Coins Data //////////////
  /////////////////////////////////

  getCoinsListRef(user: User) {
    return this.database.object(`owned-cards/${user.uid}/`);
  }



  /////////////////////////////////
  //////// Ticket Data //////////////
  /////////////////////////////////


  async submitTicket(user: User, ticket: Ticket) {
    this.ticketObject = this.database.object(`/tickets/${user.uid}/`);

    try {
      await this.database.list(`/pendingTickets/${user.uid}/`).push(ticket);
      console.log('submited')
      return true;
    }
    catch (e) {
      console.error(e);
      return false;
    }
  }

  getPendingTicketListRef(user: User): FirebaseListObservable<Ticket[]> {
    return this.database.list(`pendingTickets/${user.uid}/`)
      .map((array) => array.reverse()) as FirebaseListObservable<any[]>;
  }

  getPastTicketListRef(user: User): FirebaseListObservable<Ticket[]> {
    return this.database.list(`archivedTickets/${user.uid}/`)
      .map((array) => array.reverse()) as FirebaseListObservable<any[]>;
  }


  /////////////////////////////////
  //////// Draw Data //////////////
  /////////////////////////////////
  getLucky3Draw(ticket) {
    return this.database.object(`drawLucky3/${ticket.draw}/`);
  }


  /////////////////////////////////
  //////// Products Data //////////
  /////////////////////////////////

  getProductListRef(): FirebaseListObservable<Product[]> {
    return this.database.list(`products/`)
    //.map((array) => array.reverse()) as FirebaseListObservable<any[]>;
  }

  async submitProductExchange(user: User, product: Product) {
    this.productObject = this.database.object(`/exchanges/${user.uid}/`);

    try {
      await this.database.list(`/exchanges/${user.uid}/`).push(product);
      console.log('submited')
      return true;
    }
    catch (e) {
      console.error(e);
      return false;
    }
  }


}
