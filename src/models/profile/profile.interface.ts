export interface Profile {
    firstName: string;
    lastName: string;
    avatar: string;
    email: string;
    corpEmail: string;
    dateOfBirth: Date;
    gendre: string;
    address: string;
    address2: string;
    number: string;
    city: string;
    postcode: string;
    country: string;
    phone: number;
    language: string;
    createdAt: any;
}