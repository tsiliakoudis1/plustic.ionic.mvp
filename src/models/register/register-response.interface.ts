export interface RegisterResponse {
    result?: {
        email?: string;
        uid?: string;
    }
    error?: {
        code?: string;
        message?: string;
    }
}