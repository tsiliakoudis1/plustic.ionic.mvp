export interface Transaction {
    id?: string;
    type?: string;
    ammount?: number;
    company?: string;
    date?: string;
    time?: string;
    card?: string;
    succeed?: boolean;
}