export interface Card {
    balance?: number;
    logo?: string;
    bgColor?: string;
    image?: string;
    company?: string;
    type?: string;
    shared?: boolean;
}