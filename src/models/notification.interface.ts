export interface Notification {
    id?: string;
    type?: string;
    avatar?: string;
    name?: string;
    content?: string;
    image?: string;
    company?: string;
    date?: string;
    read?: boolean;
}