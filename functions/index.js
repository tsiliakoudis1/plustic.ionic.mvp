const functions = require('firebase-functions');
const admin = require('firebase-admin');

//////Initialize app///////
//////////////////////////////////////////
admin.initializeApp(functions.config().firebase);



//////Remove a coin after a game played///////
/////////////////////////////////////////////////
/* exports.removeCoin = functions.database.ref(`/pendingTickets/{uid}/{ticketId}`)
  .onCreate(event => {
    const ticketKey = event.data.key;
    const ticket = event.data.val();
    //I check existing coins
    var ref = admin.database().ref(`userCoins`).child(event.params.uid);

    ref.once("value", function (snapshot) {

      var user = snapshot.val();
      var currentCoins = user.coins;
      //I remove 1 coin
      newCoins = currentCoins - 1;
      admin.database().ref(`userCoins`).child(event.params.uid).child(`coins`).set(newCoins);

    }, function (errorObject) {
      console.log("The read failed at checking user coins: " + errorObject.code);
    });
  })
 */

//////Set initial coins for new accounts///////
/////////////////////////////////////////////////
/* exports.setInitialCoins = functions.auth.user().onCreate(function (event) {

  admin.database().ref(`userCoins`).child(event.data.uid).set({
    coins: 10,
  });

}); */


//////Remove coins after an exchange///////
/////////////////////////////////////////////////

/* exports.removeExchangeCoin = functions.database.ref(`/exchanges/{uid}/{exchangeId}`)
  .onCreate(event => {
    const exchangeKey = event.data.key;
    const exchange = event.data.val();

    //I check product cost
    var productCost = admin.database().ref(`exchanges`).child(event.params.uid).child(event.data.key)

    productCost.once("value", function (snapshot) {

      var exchange = snapshot.val();
      exchangeCost = exchange.cost;
      //I check existing coins
      var ref = admin.database().ref(`userCoins`).child(event.params.uid);
      ref.once("value", function (snapshot) {
        var user = snapshot.val();
        var currentCoins = user.coins;
        //I remove the product cost and set the new coins ammount
        newCoins = currentCoins - exchangeCost;
        admin.database().ref(`userCoins`).child(event.params.uid).child(`coins`).set(newCoins);
      }, function (errorObject) {
        console.log("The read failed at checking new coins: " + errorObject.code);
      });

    }, function (errorObject) {
      console.log("The read failed at product cost: " + errorObject.code);
    });
  }); */



//////Check pending Lucky3 tickets///////
/////////////////////////////////////////////////
/* exports.checkLucky3pendingTickets = functions.database.ref(`/drawLucky3/{draw}/`)
  .onCreate(event => {

    const draw = event.data.val();
    console.log(event.data.val())
    const prize3of3 = 600;
    const prize2of3 = 60;
    const prize1of3 = 6;
    var status;
    var prize;

    console.log('draw.n1 is: ' + draw.n1)
    console.log('draw.n2 is: ' + draw.n2)
    console.log('draw.n3 is: ' + draw.n3)


    var tickets = admin.database().ref(`pendingTickets`).orderByKey();
    //// I check inside pendingTickets and take snapshot
    tickets.once("value", function (snapshot) {
      var users = snapshot.val()

      //// I check inside pendingTickets and take snapshot of each user
      snapshot.forEach(function (childSnapshot) {
        var userTickets = childSnapshot.val();
        var user = childSnapshot.key
        var winnings = 0;
        //// I check inside each user and take snapshot of each ticket
        childSnapshot.forEach(function (childSnapshot) {
          var ticket = childSnapshot.val();
          var ticketKey = childSnapshot.key

          //// I set the cases of the game
          switch (true) {

            //// If 3/3
            case ticket.n1 == draw.n1 && ticket.n2 == draw.n2 && ticket.n3 == draw.n3:
              console.log('x600');
              status = "won";
              prize = prize3of3;
              break;

            //// If 1 & 2
            case ticket.n1 == draw.n1 && ticket.n2 == draw.n2:
              console.log('x60 1&2');
              winnings = winnings + prize2of3;
              status = "won";
              prize = prize2of3;
              break;

            //// If 2 & 3
            case ticket.n3 == draw.n3 && ticket.n2 == draw.n2:
              console.log('x60 2&3');
              winnings = winnings + prize2of3;
              status = "won";
              prize = prize2of3;
              break;

            //// If 1 & 3
            case ticket.n1 == draw.n1 && ticket.n3 == draw.n3:
              console.log('x60 1&3');
              winnings = winnings + prize2of3;
              status = "won";
              prize = prize2of3;
              break;

            //// If 1/3
            case ticket.n1 == draw.n1 || ticket.n2 == draw.n2 || ticket.n3 == draw.n3:
              console.log('x6');
              winnings = winnings + prize1of3;
              status = "won";
              prize = prize1of3;
              break;

            //// If 0/3
            default:
              console.log('LOST')
              status = "lost";
              prize = 0;
          }

          //// I format the checked ticket
          admin.database().ref(`archivedTickets`).child(user).push({
            draw: event.data.key,
            coinsWon: prize,
            createdAt: ticket.createdAt,
            n1: ticket.n1,
            n2: ticket.n2,
            n3: ticket.n3,
            status: status,
            date: ticket.date,
            game: "lucky3",
          }).then(function () {
            admin.database().ref(`pendingTickets`).child(user).child(ticketKey).remove()
          }, function (errorObject) {
            console.log("The remove failed: " + errorObject.code);
          });

        });

        console.log('winnings ' + winnings);

        //// I see the existing coins
        var ref = admin.database().ref(`userCoins`).child(user);
        ref.once("value").then(function (snapshot) {
          var userCoins = snapshot.val();
          var currentCoins = userCoins.coins;
          //I add the winnings and set the new coins ammount
          newCoins = currentCoins + winnings;

          admin.database().ref(`userCoins`).child(user).child(`coins`).set(newCoins)
            .then(function () {
              console.log('new coins inside then: ' + newCoins)
            })
        }, function (errorObject) {
          console.log("The read failed at case 1: " + errorObject.code);
        });

      });
    }, function (errorObject) {
      console.log("The read failed: " + errorObject.code);
    });
  });
 */


///////////////////////////////////////
//////New Lucky3 Draw///////
/////////////////////////////////////////////////
/* exports.lucky3draw = functions.https.onRequest((request, response) => {
  var n1;
  var n2;
  var n3;
  var createdAt;

  response.send("HELLO FROM Lucky3draw");
  n1 = Math.floor(Math.random() * 10);
  n2 = Math.floor(Math.random() * 10);
  n3 = Math.floor(Math.random() * 10);
  createdAt = Date.now();
  console.log(n1, n2, n3);
  console.log(createdAt)
  admin.database().ref(`drawLucky3`).push({
    n1: n1,
    n2: n2,
    n3: n3,
    createdAt: createdAt
  })

}) */


///////////////////////////////////////
//////GET FREE COINS///////
/////////////////////////////////////////////////
/* exports.getFreeCoins = functions.database.ref(`/rewards/{uid}/{rewardId}`)
  .onCreate(event => {
    const reward = event.data.val();
    const userKey = event.params.uid;

    //// I set the cases of the game
    switch (true) {

      //// ADMOB VIDEO
      case reward.type == "adMobVideo":
        console.log('adMobVideo');
        console.log(event.params.uid);

              //// I see the existing coins
              var ref = admin.database().ref(`userCoins`).child(event.params.uid);
              ref.once("value").then(function (snapshot) {
                var userCoins = snapshot.val();
                var currentCoins = userCoins.coins;
                //I add the winnings and set the new coins ammount
                newCoins = currentCoins + 1;
      
                admin.database().ref(`userCoins`).child(event.params.uid).child(`coins`).set(newCoins)
                  .then(function () {
                    console.log('new coins inside then: ' + newCoins)
                  })
              }, function (errorObject) {
                console.log("The read failed at case 1: " + errorObject.code);
              });
        break;

      //// If default
      default:
        
    }

  }) */


///////////////////////////////////////
//////GENERATE PAYCODE///////
/////////////////////////////////////////////////
exports.payCode = functions.https.onRequest((request, response) => {
  var code;
  var code2;
  var createdAt;

  response.send("HELLO FROM Plustic");
  code = Math.floor(Math.random() * (1000000)) + 111111;

  code2 = Math.random().toString().slice(2, 11);


  createdAt = Date.now();
  console.log(code, code2);
  console.log(createdAt)
  /*  admin.database().ref(`drawLucky3`).push({
     n1: n1,
     n2: n2,
     n3: n3,
     createdAt: createdAt
   }) */

})

//////SET INITIAL PAYCODE///////
/////////////////////////////////////////////////
exports.setInitialPayCode = functions.auth.user().onCreate(function (event) {
  var createdAt;
  createdAt = Date.now();
  var payCode = Math.random().toString().slice(2, 11)

  admin.database().ref(`owned-cards`).child(event.data.uid).set({
    balance: 0,
    type: "food",
    payCode: payCode
  })
    // I set a company balance for the same user to accept payments
    .then(function () {

      admin.database().ref(`companyBalance`).child(event.data.uid).set({
        balance: 0
      })
        .then(function () {
          admin.database().ref(`companyBalance`).child(event.data.uid).set({
            staffBalance: 0
          })
          console.log("Balances set")
        }, function (errorObject) {
          console.log("Set business balance error " + errorObject.code);
        })

    }, function (errorObject) {
      console.log("Set user balance error " + errorObject.code);
    });

});



///////////////////////////////////////
//////CHECK PAYMENT REQUEST///////
/////////////////////////////////////////////////
exports.checkPaymentRequest = functions.database.ref(`/pendingTransactions/{uid}/{pendingId}`)
  .onCreate(event => {
    var userKey = "";
    var userVal = [];
    varbalance = [];
    const posKey = event.params.uid;
    const request = event.data.val();
    const requestKey = event.data.key;

    console.log('Business account ID: ' + posKey)
    console.log('payCode: ' + request.payCode)
    var busBalance = admin.database().ref(`companyBalance`).child(event.params.uid);


    // I check existing business balance
    busBalance.once("value", function (snapshot) {
      balance = snapshot.val();

      console.log('this is bus balance ' + balance.balance)
    }, function (errorObject) {
      console.log("The read failed at checking business balance: " + errorObject.code);
    });



    // I check owned cards for payCode
    return admin.database().ref(`/owned-cards`)
      .orderByChild('payCode').equalTo(request.payCode)
      .once("value", function (snapshot) {
        var users = snapshot.val()
        console.log(users)

        // If there is a user with the specific payCode
        if (users) {
          snapshot.forEach(function (childSnapshot) {
            userVal = childSnapshot.val();
            userKey = childSnapshot.key
          })

          // I check if balance is enough
          if (userVal.balance >= request.total) {

            var newBalance = userVal.balance - request.total

            // I set the new owned-card balance
            admin.database().ref(`owned-cards`).child(userKey).child(`balance`).set(newBalance)

              // I set the new business balance
              .then(function () {
                var newBusBalance = balance.balance + request.total
                admin.database().ref(`companyBalance`).child(posKey).child(`balance`).set(newBusBalance)

                  // I push completed transaction
                  .then(function () {

                    admin.database().ref(`completedTransactions`).child(posKey).push({
                      createdAt: Date.now(),
                      cardID: userKey,
                      total: request.total
                    }).then(function () {

                      admin.database().ref(`completedTransactions`).child(userKey).push({
                        createdAt: Date.now(),
                        company: posKey,
                        total: request.total
                      }).then(function () {
                      // I delete pending transaction
                        admin.database().ref(`pendingTransactions`).child(posKey).child(requestKey).remove()
                      })
                    })
                  })

              }, function (errorObject) {
                console.log("set new user balance error error " + errorObject.code);
              });

            console.log("Η συναλλαγή πραγματοποιήθηκε!")
          } else {
            console.log("Η συναλλαγή απέτυχε. Ελεγξτε το υπόλοιπο της κάρτας εάν επαρκεί")
          }

        } else {
          console.log("Ο κωδικός πληρωμής δεν είναι σωστός")
        }


      }).then(function () {
        //admin.database().ref(`pendingTickets`).child(user).child(ticketKey).remove()
      }, function (errorObject) {
        console.log("payCode Snapshot function error " + errorObject.code);
      });

  })



/*   exports.payTest = functions.https.onRequest((request, response) => {
    response.send("HELLO FROM Plustic");
  })
 */



///////////////////////////////////////
//////SEND BENEFITS///////
/////////////////////////////////////////////////
exports.sendBenefits = functions.database.ref(`/benefitsRequests/{uid}/{requestid}`)
.onCreate(event => {

  var requestId = event.data.key;
  var requestObject =  event.data.val();
  var companyID =  event.params.uid;
  var ammount;
  var email;
  var profile;
  var profileKey;
  var ownCard;
  var newCredits;
  var existing;

  //// I check inside request and take the ammount to send
  var credits = admin.database().ref(`/benefitsRequests/` + companyID + `/` + event.data.key + `/credits`)
  credits.once("value", function (snapshot) {
    ammount = snapshot.val();
  })
  

  var staff = admin.database().ref(`/benefitsRequests/` + event.params.uid + `/` + event.data.key + `/users`)
  //// I check inside request and take the emails
  staff.once("value", function (snapshot) {
    users = snapshot.val();
    console.log(users)
    console.log("this is the ammount: " + ammount)

     //// I check inside emails and i do the following for each email
    snapshot.forEach(function (childSnapshot) {
      email = childSnapshot.val()
      console.log(email)

      
       // I check profiles for email
      return admin.database().ref(`/profiles`)
      .orderByChild('email').equalTo(email)
      .once("value", function (snapshot) {
        var profiles = snapshot.val();

        // If there is a user with the specific payCode
        if (profiles) {
          snapshot.forEach(function (childSnapshot) {
            profile = childSnapshot.val();
            profileKey = childSnapshot.key;
            console.log("To uid του χρήστη είναι: " + profileKey);

            var ownCard = admin.database().ref(`/owned-cards/` + profileKey)
            ownCard.once("value", function (snapshot) {
              var card = snapshot.val();
              existing = card.balance;
              console.log("this is existing credits: " + existing)
            })
            .then(function () {

            newCredits = existing + ammount;
            admin.database().ref(`owned-cards`).child(profileKey).child(`balance`).set(newCredits);  
            console.log("this is new credits: " + newCredits)          

            })

          })
        } else {
          console.log("Ο χρήστης με το email " + email + " δεν βρέθηκε.")
        }

      }).then(function () {
        //admin.database().ref(`pendingTickets`).child(user).child(ticketKey).remove()
      }, function (errorObject) {
        console.log("profile snapshot function error " + errorObject.code);
      });












      
    })
  })





})
